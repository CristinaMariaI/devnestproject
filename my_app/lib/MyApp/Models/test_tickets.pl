#!/usr/bin/perl
use strict;
use warnings;
use lib "/home/dell/devnestproject/my_app/lib/MyApp/Models/";
use Tickets;

my $ticket = Tickets -> new({tid => "1", 
name => "Ticket 1", 
description => "t_model", 
status => "almost done",
date => "january 19"});

print ($ticket -> get_tid());
$ticket -> set_tid(10);
print ("\n");
print ($ticket -> get_tid());
print ("\n");
print ($ticket -> get_name());
$ticket -> set_name("ticket 10");
print ("\n");
print ($ticket -> get_name());
print ("\n");
print ($ticket -> get_description());
$ticket -> set_description("t_model10");
print ("\n");
print ($ticket -> get_description());
print ("\n");
print ($ticket -> get_status());
$ticket -> set_status("done");
print ("\n");
print ($ticket -> get_status());
print ("\n");
print ($ticket -> get_date());
$ticket -> set_status("january 20");
print ("\n");
print ($ticket -> get_date());
