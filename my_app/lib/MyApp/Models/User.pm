package MyApp::Models::User;

use strict;
use warnings;

our $AUTOLOAD;

sub new {

    my ( $class, $args ) = @_;

    my $self = {
        
        id => $args->{id},
        role => $args->{role},
        first_name => $args->{first_name},
        last_name => $args->{last_name},
        password => $args->{password},
        email => $args->{email},

    };

    bless $self, $class;
    return $self;

}

sub AUTOLOAD {

    my ( $self, $arg) = @_;

    my $name = $AUTOLOAD;  
	$name =~ s/.*\://;

    if (defined $arg){

        $self->{$name} = $arg;

    } else {

        return $self->{$name};
    }
}

sub is_admin {

    my $self = shift;
    
    if ( $self->{role} == 2) {
        return 0;
    }elsif ( $self->{role} == 1 ){
        return 1;
    }
    
}

sub find_by_id {

    my ( $self, $id ) = @_;

    my $dbh = MyApp::Repository::Database::connect_db();

    my $sth = $dbh->prepare('SELECT * FROM Users WHERE id=?;');
    
    local $@; undef $@;

    eval {
        $sth->execute($id) or die $DBI::errstr;
        1;
    };

    if ( $@ ) {
        warn "Something was wrong with the db operation: $@ \n";
        return undef; 
    }

    my $row = $sth->fetchrow_hashref();
    
    my $user =  MyApp::Models::User->new($row);
    
    return $user;

}

sub get_all {

    my $self = shift;

    my $dbh = MyApp::Repository::Database::connect_db();

    my $sth = $dbh->prepare('SELECT * FROM Users;');
    
    local $@; undef $@;

    eval {
        $sth->execute() or die $DBI::errstr;
        1;
    };

    if ( $@ ) {
        warn "Something was wrong with the db operation: $@ \n";
        return []; 
    }
  
    my @rows;

    while ( my $row = $sth->fetchrow_hashref() ) {

     push( @rows, $row ); 

    };

    return \@rows;
}

1;
