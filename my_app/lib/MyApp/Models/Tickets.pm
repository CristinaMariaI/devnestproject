package MyApp::Models::Tickets;

sub new {
 my ( $class, $args ) = @_;

 my $self = bless {
   tid => $args->{tid},
   name => $args->{name},
   description => $args->{description},
   status => $args->{status},
   date => $args->{date},
 }, $class;

return $self;

}

sub get_tid {
 my $self = shift;
 return $self->{tid};
}

sub set_tid {
 my ( $self, $tid ) = @_;
 $self->{tid} = $tid;
}

sub get_name {
 my $self = shift;
 return $self->{name};
}

sub set_name {
 my ( $self, $name ) = @_;
 $self->{name} = $name;
}

sub get_description {
 my $self = shift;
 return $self->{description};
}

sub set_description {
 my ( $self, $description ) = @_;
 $self->{description} = $description;
}

sub get_status {
 my $self = shift;
 return $self->{status};
}

sub set_status {
 my ( $self, $status ) = @_;
 $self->{status} = $status;
}

sub get_date {
 my $self = shift;
 return $self->{date};
}

sub set_date {
 my ( $self, $date ) = @_;
 $self->{date} = $date;
}
1;