package MyApp::Repository::Database;

use Mojo::Base 'Mojolicious';
use Mojolicious::Plugins;
use DBI;
use strict;

sub connect_db {

  my $self = shift;

  my $app = MyApp ->new();
  my $config = $app->plugin('Config');
  my $driver = "SQLite";
   my $database1 = $config -> {database} -> [0];

   my $dsn = "DBI:$driver:dbname=$database1";
     print "DSN: $dsn\n";

   my $userid= "";
   my $password = "";
   my $dbh = DBI -> connect($dsn, $userid, $password, { RaiseError => 1})
     or die $DBI::errstr;

   return $dbh;
  # CONFIG VARIABLES
# my $database = $config -> {database};
# my $host = $config -> {host};
# my $port = "3306";
# my $user = $config -> {user};
# my $pw = $config -> {pw};

# #DATA SOURCE NAME
# my $dsn = "dbi:mysql:$database:$host:$port";

# # PERL DBI CONNECT
# my $DBIconnect = DBI->connect($dsn, $user, $pw);
}

1;