package MyApp::Repository::UserRepository;
use MyApp::Repository::Database;
use MyApp::Controller::RegistrationController;
use MyApp::Models::User;
use Mojo::Log;

sub add_new_user {

  my ($role, $first_name, $last_name, $password, $email) = @_;
      
  my $dbh = MyApp::Repository::Database::connect_db();
  
  my $sth = $dbh->prepare('INSERT OR IGNORE INTO Users(role,first_name,last_name,password,email) VALUES (?, ?, ?, ?, ?)');
  my $val = $sth->execute($role, $first_name, $last_name, $password, $email);


  return $val;

}

sub validate_user {

  my ($email, $password) = @_;

  my $dbh = MyApp::Repository::Database::connect_db();

  my $sth = $dbh->prepare('SELECT * FROM Users WHERE email = ?');
  my $rv = $sth->execute($email) or die $DBI::errstr;

  my $row = $sth->fetchrow_hashref();

# Log to STDERR
  # my $log = Mojo::Log->new;

  # # Customize log file location and minimum log level
  # my $log = Mojo::Log->new(path => '/home/dell/devnestproject/my_app/lib/MyApp/Repository/UserRepository.log', level => 'warn');
  # $log->warn('Not sure what is happening here');
  return $row;
  
}

sub validate_password {

  my ($email, $password) = @_;

  my $dbh = MyApp::Repository::Database::connect_db();

  my $sth = $dbh->prepare('SELECT password FROM Users WHERE email = ?');
  my $rv = $sth->execute($email) or die $DBI::errstr;

  my $pass = $sth->fetchrow_array();

  if ($password ne $pass){

    return 0;

  } else {
    
    return 1;
  }
}

sub update_user {

    my ($id, $args) = @_;

    my $dbh = MyApp::Repository::Database::connect_db();

    my $sth = $dbh->prepare('UPDATE Users
                             SET role = ?,
                                first_name = ?,
                                last_name = ?,
                                email = ?
                             WHERE id = ?;');
   
    eval {
        $sth->execute($args->{role}, $args->{first_name}, $args->{last_name}, $args->{username}, $id) or die $DBI::errstr;
        1;
      };
    
    if ( $@ ) {
        warn "Something was wrong with the db operation: $@ \n";
        return undef; 
      };

  }

1;
