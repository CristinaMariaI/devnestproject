package MyApp::Controller::RegistrationController;
use Mojo::Base 'Mojolicious::Controller';
use Crypt::PBKDF2;
use MyApp::Repository::UserRepository;
use MyApp::Models::User;


sub register {

  my $c = shift;

  my $session = $c->session;
  my $logged_user_id = $session->{user_id};
  my $logged_user = MyApp::Models::User->find_by_id($logged_user_id);

  if ( !$logged_user->is_admin() ){

    $c->flash(error=>'You have no access to this feature!');
    return $c->redirect_to('welcome');

  } 

   $c->render (template => 'register',
      message => $c->flash('message'),
      error => $c->flash('error'),
    );

  #in this way we point directly to register.html.ep
}

sub registration {

  my $c= shift;

  my $email = $c->param('username');
  my $password = $c->param('password');
  my $confirm_password = $c->param('confirm_password');
  my $first_name = $c->param('first_name');
  my $last_name = $c->param('last_name');
  my $user = $c->param('flexRadioDefault1');
  my $admin = $c->param('flexRadioDefault2');


   if(! $email || ! $password || ! $confirm_password || ! $first_name || ! $last_name){

    $c->flash(error => 'Username, Password, First Name and Last Name are the mandatory fields!');
    $c->redirect_to('register');

  }

  if($password ne $confirm_password) {
    $c->flash( error => 'Password and Confirm Password must be the same!');
    $c->redirect_to('register');
  }

  my $role;
  if ( $admin ){
     $role = 1;
  } elsif ( $user ) {
     $role = 2; 
  }

  my $dbh = MyApp::Repository::UserRepository::add_new_user($role, $first_name, $last_name, $password, $email);

    if( $dbh != 1 ) {

    $c->flash(error => 'The e-mail address is already used!'); 
    $c->redirect_to('register');
     
  }

  $c->flash(message=>'Registration successful!');
  $c->redirect_to('register');

}

sub generate_password {
  
  my $pass=shift;

  my $pbkdf2 = Crypt::PBKDF2->new (
    hash_class=>'HMACSHA1',
    iterations => 1000,
    output_len => 20,
    salt_len => 4,
  );

  return $pbkdf2->generate($pass);
}

1;
