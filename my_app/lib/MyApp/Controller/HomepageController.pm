package MyApp::Controller::HomepageController;
use Mojo::Base 'Mojolicious::Controller';



sub homepage {

  my $c = shift;
  
  $c -> render (template => 'homepage',
  error => $c->flash('error'),
  message => $c->flash('message')
  );

}

1;