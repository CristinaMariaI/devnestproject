package MyApp::Controller::ViewUserController;
use Mojo::Base 'Mojolicious::Controller';
use MyApp::Models::User;


sub viewuser {

  my $c = shift;

  my $session = $c->session;
  my $logged_user_id = $session->{user_id};
  my $logged_user = MyApp::Models::User->find_by_id($logged_user_id);

  if ( !$logged_user->is_admin() ){

    $c->flash(error=>'You have no access to this feature!');
    return $c->redirect_to('welcome');
  
  } 

  my $id = $c->req->query_params->param('id');
  
  my $user = MyApp::Models::User->find_by_id($id);

  $c->stash(user_object => $user);

  $c->render(template => 'viewuser');


}


1;
