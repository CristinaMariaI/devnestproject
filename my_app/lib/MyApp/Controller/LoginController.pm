package MyApp::Controller::LoginController;
use Mojo::Base 'Mojolicious::Controller';
use MyApp::Repository::UserRepository;

sub login {

  my $c = shift;

  $c -> render (template => 'login',
  error => $c->flash('error'),
  message => $c->flash('message')
  );
  #in this way we point directly to login.html.ep
}

sub logout {
  my $c = shift;

  my $session = $c->session;
  delete $session->{user_id};

  return login($c);
}

sub log_in {

  my $c = shift;

  my $email = $c->param('username');
  my $password = $c->param('password');
  my $user = $c -> param('flexRadioDefault1');
  my $admin = $c ->param('flexRadioDefault2');

  if (! $email || ! $password ){

    $c->flash(error => 'Username and Password are the mandatory fields!');
    $c->redirect_to('login');

  }

  my $user_info = MyApp::Repository::UserRepository::validate_user($email, $password);
  my $pass = MyApp::Repository::UserRepository::validate_password($email, $password);

  if (! $user_info){

    $c->flash(error => 'Username entered is not registered!');
    $c->redirect_to('login');

  } elsif ($pass == 0){

    $c->flash(error => 'Incorrect password!');
    $c->redirect_to('login');

  } else {
    my $session = $c->session;
    $session->{user_id} = $user_info->{id};

    $c->redirect_to('welcome');

  }

}

1;