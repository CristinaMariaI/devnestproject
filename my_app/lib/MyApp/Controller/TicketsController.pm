package MyApp::Controller::TicketsController;
use Mojo::Base 'Mojolicious::Controller';


sub tickets {
  my $c = shift;
  $c->render (template => 'tickets',
  error => $c->flash('error'),
  message => $c->flash('message')
  );

}


1;