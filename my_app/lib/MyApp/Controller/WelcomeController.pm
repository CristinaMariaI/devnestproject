package MyApp::Controller::WelcomeController;
use Mojo::Base 'Mojolicious::Controller';
use MyApp::Repository::UserRepository;
#use lib "/home/dell/devnestproject/my_app/lib/MyApp/Models/";
use MyApp::Models::Tickets;

sub welcome {
  my $c = shift;

  # This should be taken from DB, based on the user id from the session
  my $user_is_admin = 0;

  #This is how we pass perl variables in the template
  $c->stash(user_is_admin => $user_is_admin);

  my $ticket1 = MyApp::Models::Tickets->new(
    {  
      tid => '1',
      name => 'Register/Login',
      description => 'Authentification+Login/Logout',
      status => 'solved',
      date =>'december 2021',
    });
  my $ticket2 = MyApp::Models::Tickets->new ({
      tid => '2',
      name => 'Ticketing dashboard',
      description => 'Ticket model(all the needed functions)',
      status => 'unsolved',
      date =>'due ...',
    });
  my $ticket3 = MyApp::Models::Tickets->new ({
      tid => '3',
      name => 'User`s list',
      description => 'User model( the functions and the route to the list)',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket4 = MyApp::Models::Tickets->new ({
      tid => '4',
      name => ' User concerns ',
      description => 'Add user form+UI',
      status => 'solved',
      date =>'december 2021',
    });
  my $ticket5 = MyApp::Models::Tickets->new ({
      tid => '5',
      name => 'Ticketing system',
      description => 'Add ticket form+UI',
      status => 'unsolved',
      date =>'due ...',
    });
  my $ticket6 = MyApp::Models::Tickets->new ({
      tid => '6',
      name => 'Update User',
      description => 'Update user requierment( admin only) + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket7 = MyApp::Models::Tickets->new ({
      tid => '7',
      name => 'Update Ticket',
      description => 'Update ticket requierment + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket8 = MyApp::Models::Tickets->new ({
      tid => '8',
      name => 'ViewUser',
      description => 'View user + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket9 = MyApp::Models::Tickets->new ({
      tid => '9',
      name => 'ViewTicket',
      description => 'View ticket + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket10 = MyApp::Models::Tickets->new ({
      tid => '10',
      name => 'DeleteU',
      description => 'Delete certain user + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket11 = MyApp::Models::Tickets->new ({
      tid => '11',
      name => 'Reply',
      description => 'Add reply to certain ticket + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
    my $ticket12 = MyApp::Models::Tickets->new ({
      tid => '12',
      name => 'Changes',
      description => 'Change status for some ticket + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket13 = MyApp::Models::Tickets->new ({
      tid => '13',
      name => 'ChangesDev',
      description => 'Change Dev for some ticket + UI',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket14 = MyApp::Models::Tickets->new ({
      tid => '14',
      name => 'User/Admin',
      description => 'Users access to admin only',
      status => 'unsolved',
      date =>'due ... ',
    });
  my $ticket15 = MyApp::Models::Tickets->new ({
      tid => '15',
      name => 'ShowT',
      description => 'List all tickets + filter by status',
      status => 'Partially solved',
      date =>'january 2022',
    });
  my $ticket16 = MyApp::Models::Tickets->new ({
      tid => '16',
      name => 'Changes3',
      description => 'Modify registered users',
      status => 'unsolved',
      date =>'due ... ',
    });

  my $tickets = [
     $ticket1,
     $ticket2,
     $ticket3,
     $ticket4,
     $ticket5,
     $ticket6,
     $ticket7,
     $ticket8,
     $ticket9,
     $ticket10,
     $ticket11,
     $ticket12,
     $ticket13,
     $ticket14,
     $ticket15,
     $ticket16

  ];
   

  $c->stash(tickets => $tickets);

  $c->render (template => 'welcome');
}


1;