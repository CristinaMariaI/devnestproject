package MyApp::Controller::EditUserController;
use Mojo::Base 'Mojolicious::Controller';
use MyApp::Repository::UserRepository;
use MyApp::Models::User;


sub edituser {

  my $c = shift;

  my $session = $c->session;
  my $logged_user_id = $session->{user_id};
  my $logged_user = MyApp::Models::User->find_by_id($logged_user_id);

  if ( !$logged_user->is_admin() ){

    $c->flash(error=>'You have no access to this feature!');
     return $c->redirect_to('welcome');
  
  } 

  my $id = $c->req->query_params->param('id');
  
  my $user = MyApp::Models::User->find_by_id($id);

  $c->stash(user_object => $user);
 
  $c->render (template => 'edituser');


}

sub edit_user {

  my $c = shift;
   
  my $id = $c->req->query_params->param('id');
  my $user = MyApp::Models::User->find_by_id($id);

  my $user_info = {

    first_name => $user->{first_name},
    last_name => $user->{last_name},
    username => $user->{email},
    role => $user->{role},

  };

  my @fields = ("first_name", "last_name", "username", "userbutton", "adminbutton");
  my $data = {};

  foreach my $field (@fields){

    if ( $c->param($field) ){ 

      if ($field eq "userbutton"){
        $data->{role} = 2;
      } elsif ($field eq "adminbutton"){
        $data->{role} = 1;
      } else {
        $data->{$field} = $c->param($field);
      }
    
    }

  };

  foreach my $key (keys %$user_info){

    if ( !$data->{$key} ) {

      $data->{$key} = $user_info->{$key};

    }
  };


  MyApp::Repository::UserRepository::update_user($id, $data);

  $c->redirect_to('users');
  

}


1;
