package MyApp::Controller::UsersController;
use Mojo::Base 'Mojolicious::Controller';
use MyApp::Models::User;
use MyApp::Repository::UserRepository;


sub users {

  my $c = shift;
  
  my $session = $c->session;
  my $user_id = $session->{user_id};
  
  my $user = MyApp::Models::User->find_by_id($user_id);

  my $user_is_admin = $user->is_admin();

  if ( !$user_is_admin ) {

    $c->flash(error=>'You have no access to this feature!');
    return $c->redirect_to('welcome');
  
  } 
  
  my $rows = $user->get_all();
  $c->stash(rows => $rows);
  $c->stash(user_is_admin => $user_is_admin);
  $c->render (template => 'users');
  
  

}



1;
