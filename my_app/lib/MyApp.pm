package MyApp;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {

  my $self = shift;

  # Load configuration from hash returned by config file
  my $config = $self->plugin('Config');

  # Configure the application
  $self->secrets($config->{secrets});

   # Router
  my $r = $self->routes;
  $r->get('/')->to(controller=>'HomepageController', action => 'homepage');
  $r->post('/login')->to(controller => 'LoginController', action => 'log_in');
  $r->get('/login')->to(controller => 'LoginController', action => 'login');
  


  # Defining an 'under' route, this helps because we want to check if the user is logged in order
  # to access certain parts of our application
  my $auth = $r->under('/' => sub {
    my $self = shift;
    
    my $session = $self->session;
    
    if ( defined $session->{user_id} ) {
      return 1;
    }

    $self->render (template => 'login',
      error => $self->flash('error'),
      message => $self->flash('message')
    );

    return undef;
  });

  
  $auth->get('/logout')->to(controller=>'LoginController', action => 'logout');

  $auth->post('/register')->to(controller => 'RegistrationController', action => 'registration');
  $auth->get('/register')->to(controller => 'RegistrationController', action => 'register');
  $auth->get('/welcome')->to(controller=>'WelcomeController', action => 'welcome');
  $auth->get('/users')->to(controller=>'UsersController', action => 'users');
  $auth->get('/edituser')->to(controller=>'EditUserController', action => 'edituser');
  $auth->post('/edituser')->to(controller=>'EditUserController', action => 'edit_user');
  $auth->get('/viewuser')->to(controller=>'ViewUserController', action => 'viewuser');

 

 
  #when the user hits/register route => it goes directly to the RegistrationController
  #and executes what is already created there.

  
}

1;
